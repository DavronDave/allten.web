<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\Basic\LangController;
use App\Http\Controllers\Admin\Basic\ContactController;
use App\Http\Controllers\Admin\Basic\AboutController;
use App\Http\Controllers\Admin\Request\RequestController;
use App\Http\Controllers\Admin\Dashboard\DashboardController;
use App\Http\Controllers\Admin\User\SettingController;

use App\Http\Controllers\Site\Home\HomeController;
use App\Http\Controllers\Site\About\AboutController as SiteAboutController;
use App\Http\Controllers\Admin\Contact\ContactController as SiteContactController;
use App\Http\Controllers\Site\Team\TeamController as SiteTeamController;
use App\Http\Controllers\Site\Service\ServiceController as SiteServiceController;
use App\Http\Controllers\Site\Blog\BlogController as SiteBlogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Site routes
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'siteVisits']
    ], function () {

    Route::name('site.')->group(function() {
        Route::get('/', [HomeController::class, 'index'])->name('index');
        Route::get('/about', [SiteAboutController::class, 'index'])->name('about');
        Route::get('/contacts', [SiteContactController::class, 'index'])->name('contact');
        Route::get('/team', [SiteTeamController::class, 'index'])->name('team');
        Route::get('/services', [SiteServiceController::class, 'index'])->name('service');
        Route::get('/service/details', [SiteServiceController::class, 'details'])->name('service-details');
        Route::get('/blog', [SiteBlogController::class, 'index'])->name('blog');
        Route::get('/blog/details', [SiteBlogController::class, 'details'])->name('blog-details');
        Route::get('/product/{id}', [SiteProductController::class, 'show'])->name('product');
        Route::get('/search', [HomeController::class, 'searchData'])->name('search');
    });
});

// Admin routes
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LoginController::class, 'logout'])->name('logout');
Route::get('/admin', function () {
    return redirect('admin/home');
});
Route::prefix('/admin')->name('admin.')->middleware('role:admin')->group(function() {

    Route::get('/home', [DashboardController::class, 'index'])->name('index');

    Route::get('setting/edit',[SettingController::class, 'edit'])->name('setting.edit');
    Route::post('setting/update',[SettingController::class, 'update'])->name('setting.update');

    // Languages
    Route::prefix('languages')->name('language.')->group(function(){
        Route::get('/index', [LangController::class, 'index'])->name('index');
        Route::get('/show/{lang}', [LangController::class, 'show'])->name('show');
        Route::get('/edit/{lang}', [LangController::class, 'edit'])->name('edit');
        Route::get('/create', [LangController::class, 'create'])->name('create');
        Route::post('/store', [LangController::class, 'store'])->name('store');
        Route::post('/update/{lang}', [LangController::class, 'update'])->name('update');
    });

    // Contacts
    Route::prefix('contacts')->name('contact.')->group(function(){
        Route::get('/index', [ContactController::class, 'index'])->name('index');
        Route::get('/edit/{contact}', [SiteContactController::class, 'edit'])->name('edit');
        Route::post('/store', [SiteContactController::class, 'store'])->name('store');
        Route::post('/update/{contact}', [SiteContactController::class, 'update'])->name('update');
    });

    // About
    Route::prefix('about')->name('about.')->group(function(){
        Route::get('/index', [AboutController::class, 'index'])->name('index');
        Route::get('/edit', [AboutController::class, 'edit'])->name('edit');
        Route::post('/update', [AboutController::class, 'update'])->name('update');
    });

    // Requests
    Route::prefix('requests')->name('request.')->group(function(){
        Route::get('/index', [RequestController::class, 'index'])->name('index');
        Route::get('/show/{request}', [RequestController::class, 'show'])->name('show');
    });
});
