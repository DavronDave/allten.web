<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('contacts')->delete();

        \DB::table('contacts')->insert(array (
            0 =>
                array (
                    'created_at' => '2023-06-17 10:24:55',
                    'title'      => 'Biz bilan bog\'lanish',
                    'description'=> 'Agar sizda qandaydir savol bo\'lsa bizga murojaat qilishingiz mumkin',
                    'phone'      => '+998944765241',
                    'address'    => 'Toshkent sh. Chilonzor t. 12 A',
                    'email'      => 'davronomonov99@gmail.com',
                ),
        ));
    }
}
