<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MapTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('map')->delete();

        \DB::table('map')->insert(array (
            0 =>
                array (
                    'point_x' => '69.2792398975887',
                    'point_y' => '41.313434899829836',
                ),
        ));
    }
}
