<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RequestsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('requests')->delete();

        \DB::table('requests')->insert(array (
            0 =>
            array (
                'name'      => 'Davron',
                'email'     => 'davronomonov99@gmail.com',
                'phone'     => '+998944765241',
                'subject'   => 'Buyurtma berish',
                'message'   => 'Assalomu alekum qanday buyurtma bersam bo\'ladi',
                'status'    => '1',
                'created_at'=> '2023-06-17 15:26:50',
                'updated_at'=> '2023-06-17 15:26:50',
            ),
        ));


    }
}
