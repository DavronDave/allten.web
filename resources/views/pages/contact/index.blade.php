@extends('layouts.default')

@section('title', 'Контакты')

@push('css')
	<link href="/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" />
	<link href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
    <link href="/assets/plugins/blueimp-gallery/css/blueimp-gallery.min.css" rel="stylesheet"/>
    <link href="/assets/plugins/blueimp-file-upload/css/jquery.fileupload.css" rel="stylesheet"/>
    <link href="/assets/plugins/blueimp-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
@endpush

@section('content')
	<!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="#">Рабочий стол</a></li>
        <li class="breadcrumb-item active">Контакты</li>
    </ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Контакты</h1>
	<!-- end page-header -->
	<!-- begin panel -->

	<div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title"></h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
            </div>
        </div>
		<!-- begin panel-body -->
		<div class="panel-body">
			<table id="data-table" class="table table-striped table-bordered table-td-valign-middle ">
				<thead>
					<tr>
						<th width="1%">#</th>
						<th class="text-nowrap">Название</th>
						<th class="text-nowrap">Описани:</th>
						<th class="text-nowrap">Э-почта</th>
						<th class="text-nowrap">Тел.</th>
						<th class="text-nowrap">Действие</th>
					</tr>
				</thead>
				<tbody>
                    @foreach($contacts as $contact)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $contact->title }}</td>
                            <td>{{ $contact->description }}</td>
                            <td>{{ $contact->email }}</td>
                            <td>{{ $contact->phone }}</td>
                            <td align="center">
                                <a href="{{route('admin.contact.edit', ['contact' => $contact->id])}}" class="btn btn-icon btn-primary"><i class="fas fa-pencil-alt"></i></a>
                            </td>
                        </tr>
                    @endforeach
				</tbody>
			</table>
            <hr class="m-b-50">
            <div class="row mb-3">
                <div class="col-12">
                    <h6>Расположение на карте</h6>
                    <div id="map" style="height: 300px"></div>
                </div>
            </div>
		</div>
        </div>
        <!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
	<script src="/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <script src="/assets/plugins/blueimp-file-upload/js/jquery.fileupload.js"></script>
    <script src="/assets/plugins/blueimp-file-upload/js/jquery.fileupload-image.js"></script>
    <script>
        document.getElementById('image').onchange = function (evt) {
            let tgt = evt.target || window.event.srcElement,
                files = tgt.files;
            // FileReader support
            if (FileReader && files && files.length) {
                let fr = new FileReader();
                fr.onload = function () {
                    document.getElementById('image-src').src = fr.result;
                }
                fr.readAsDataURL(files[0]);
            }
            // Not supported
            else {
            }
        }
    </script>
    <script>
        if ($('#data-table-fixed-columns').length !== 0) {
            $('#data-table-fixed-columns').DataTable({
                scrollY:        300,
                scrollX:        true,
                scrollCollapse: true,
                fixedColumns: true,
            });
        }
    </script>
    <script>
        function changeTypeName(element) {
            let name =  document.getElementById('name')
            if (element.value === 'social') {
               name.value = ''
                $("#name").prop('readonly', false);
            } else {
                name.value = element.options[element.selectedIndex].text
                $("#name").prop('readonly', true);
            }
        }
    </script>

   {{-- <script src="https://api-maps.yandex.ru/2.1/?apikey=72136f46-adf8-474e-a9b7-d5d5d9600b81&lang=ru_RU"
            type="text/javascript"></script>
    <script type="text/javascript">
        ymaps.ready(init);

        function init() {
            let myMap = new ymaps.Map("map", {
                center: [{{$map['point_y']}}, {{$map['point_x']}}],
                zoom: 14,
                draggable: true,
            });
            const collection = new ymaps.GeoObjectCollection(null, {
                preset: 'twirl#redIcon',
                iconContent: ' ',
                hintContent: "",
                balloonContent: "Содержимое <em>балуна</em> метки"
            });
            collection.add(new ymaps.Placemark([{{$map->point_y}}, {{$map->point_x}}], {
                iconContent: ' ',
                hintContent: "",
                balloonContent: "Содержимое <em>балуна</em> метки"
            }));
            myMap.geoObjects.add(collection);
        }
    </script>--}}
@endpush
