@extends('layouts.default')

@section('title', 'О проекте')

@section('content')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Рабочий стол</a></li>
        <li class="breadcrumb-item active">О проекте</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">О проекте</h1>
    <!-- end page-header -->
    <!-- begin panel -->
    <div class="panel panel-inverse" style="background: #f4f4f7">
        <!-- begin panel-heading -->
        <div class="panel-heading">
            <h4 class="panel-title"></h4>
            <div class="panel-heading-btn">
                <a href="{{route('admin.about.edit')}}" class="btn btn-xs btn-primary mr-3">
                    <i class="fa fa-pencil-alt"></i> Редактировать
                </a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
            </div>
        </div>
        <!-- end panel-heading -->

        <form>
            <div class="panel-body p-t-10">
                <div class="row">
                    <!-- begin col-6 -->
                    <div class="col-xl-12">
                        <!-- begin nav-tabs -->
                        <ul class="nav nav-pills m-10">
                            @foreach($languages as $lang)
                                <li class="nav-item">
                                    <a href="#tab-desc-{{$lang['url']}}" data-toggle="tab"
                                       class="nav-link{{(!$loop->index)?' active':''}}">
                                        <span class="d-sm-none">{{ $lang['name'] }}</span>
                                        <span class="d-sm-block d-none">{{ $lang['name'] }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                        <!-- end nav-tabs -->
                        <!-- begin tab-content -->

                        <div class="tab-content">
                            @foreach($languages as $lang)
                                <!-- begin tab-pane -->
                                <div class="tab-pane fade{{(!$loop->index)?' active show':''}}" id="tab-desc-{{$lang['url']}}">
                                    <label for="title[{{$lang['url']}}]" class="font-weight-bold">Заголовок:</label>
                                    <input type="text" name="title[{{$lang['url']}}]" id="title[{{$lang['url']}}]" readonly
                                           placeholder="Введите баннер" class="form-control mb-3"
                                           value="{!! $item['title'][$lang->url] !!}">

                                    <label for="description[{{$lang['url']}}]" class="font-weight-bold">Описание:</label>
                                    <textarea class="form-control mb-3" name="description[{{$lang->url}}]" rows="12" readonly>{!! strip_tags($item['description'][$lang->url]) !!}</textarea>

                                </div>
                                <!-- end tab-pane -->
                            @endforeach
                        </div>
                    </div>
                    <!-- end col-6 -->
                    <!-- begin col-6 -->
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="" >
                                <img class="img-thumbnail" src="{{ $item['image']}}" id="image-src" style="height: 150px; width: 150px"  >
                            </div>

                        </div>
                        <div class="col-xl-6">
                            <div class="" >
                                <img class="img-thumbnail" src="{{ $item['logo']}}" id="image-src" style="height: 150px; width: 150px"  >
                            </div>
                        </div>
                    </div>
                    <!-- end col-6 -->
                </div>

                <div class="float-right m-t-5">
                    <a href="{{route('admin.about.edit')}}" class="btn btn-primary mr-3">
                        <i class="fa fa-pencil-alt"></i> Редактировать
                    </a>
                </div>
            </div>
        </form>
    </div>



    <!-- end panel-body -->
    </div>
    <!-- end panel -->
@endsection

@push('scripts')

@endpush

