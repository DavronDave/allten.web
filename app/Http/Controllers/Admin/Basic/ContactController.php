<?php

namespace App\Http\Controllers\Admin\Basic;

use App\Http\Controllers\Controller;
use App\Models\Admin\Basic\Map;
use App\Models\Basic\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contact::all();
       // $map  = Map::all();
        //dd($contacts);
        return view('pages.contact.index',compact('contacts'));
    }
}
