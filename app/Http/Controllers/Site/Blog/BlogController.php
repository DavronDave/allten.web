<?php

namespace App\Http\Controllers\Site\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        return view('site.blogs.blog');
    }
    public function details()
    {
        return view('site.blogs.blog-details');
    }
}
