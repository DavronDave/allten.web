<?php

namespace App\Http\Controllers\Site\Service;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function index()
    {
        return view('site.services.services');
    }

    public function details()
    {
        return view('site.services.service-details');
    }
}
