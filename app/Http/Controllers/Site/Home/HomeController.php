<?php

namespace App\Http\Controllers\Site\Home;

use App\Http\Controllers\Controller;
use App\Models\Banner\Banner;
use App\Models\Basic\About;
use App\Models\Methods\PublicMethod;
use App\Models\Product\Product;
use App\Models\Basic\Lang;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('site.dashboard.index');
    }

    public function searchData(Request $request)
    {
        $response = PublicMethod::searchContent();
        return view('site.search', compact('response'));
    }
}
