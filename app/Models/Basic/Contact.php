<?php

namespace App\Models\Basic;

use App\Models\Methods\PublicMethod;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    public static function getData()
    {
        $item = Contact::all()->first();
        if (is_null($item)) {
            $item = [
                "id" => 1,
                "name" => null,
                "email" => null,
                "phone" => null,
                "subject" => null,
                "message" => null,
                "point_x" => null,
                "point_y" => null,
                "created_at" => null,
                "updated_at" => null
            ];
        } else {
            $item = $item->toArray();
        }
        return $item;
    }
}
